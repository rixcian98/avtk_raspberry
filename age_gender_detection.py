# Import důležitých knhoven
from picamera.array import PiRGBArray
from picamera import PiCamera
import imutils
import time
import cv2
import requests
from grovepi import *

# Inicializace kamery a nastevení obrazového vstupu z kamery 
camera = PiCamera()
camera.resolution = (640, 480)
camera.framerate = 32
rawCapture = PiRGBArray(camera, size=(640, 480))

# Definice digitálního výstupu LED diod
ledM = 2
ledC = 3

# Množina výsledných hodnot
MODEL_MEAN_VALUES = (78.4263377603, 87.7689143744, 114.895847746)
age_list=['(0, 2)','(4, 6)','(8, 12)','(15, 20)','(25, 32)','(38, 43)','(48, 53)','(60, 100)']
gender_list = ['muz', 'zena']

# Reset minimálního a maximální věku
min_age = 0
max_age = 0
 
# Pozastavíme program z důvodu nastartování kamery
time.sleep(0.1)
 
# Inicializace vytrénovaného modelu 
def initialize_caffe_model():
    print('Loading models...')
    age_net = cv2.dnn.readNetFromCaffe(
                        "age_gender_model/deploy_age.prototxt", 
                        "age_gender_model/age_net.caffemodel")
    gender_net = cv2.dnn.readNetFromCaffe(
                        "age_gender_model/deploy_gender.prototxt", 
                        "age_gender_model/gender_net.caffemodel")
 
    return (age_net, gender_net)

# Cyklus, ktery ziskava obraz z kamery a dále ho zpracovává
def capture_loop(age_net, gender_net): 
    font = cv2.FONT_HERSHEY_SIMPLEX
    digitalWrite(ledM,0)
    digitalWrite(ledC,0)
    # zachycení snímku z kamery
    for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
        # Prevod obrazku na numpy pole
        image = frame.array
       
        face_cascade = cv2.CascadeClassifier('/usr/local/share/OpenCV/haarcascades/haarcascade_frontalface_alt.xml')
        gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray, 1.1, 5)
        print("Found "+str(len(faces))+" face(s)")
 
        #Nakresli velde kazdeho obliceje cetverec
        for (x,y,w,h) in faces:
            cv2.rectangle(image,(x,y),(x+w,y+h),(255,255,0),2)
            face_img = image[y:y+h, x:x+w].copy()
            blob = cv2.dnn.blobFromImage(face_img, 1, (227, 227), MODEL_MEAN_VALUES, swapRB=False)
            # Odhadnutí pohlaví
            gender_net.setInput(blob)
            gender_preds = gender_net.forward()       
            gender = gender_list[gender_preds[0].argmax()]
            print(gender)
            # Odhadnutí věku
            age_net.setInput(blob)
            age_preds = age_net.forward()
            age = age_list[age_preds[0].argmax()]
            print(age)
            overlay_text = "%s, %s" % (gender, age)
            cv2.putText(image, overlay_text ,(x,y), font, 2,(255,255,255),2,cv2.LINE_AA)
            # nastavení hranic věku
            if age_preds[0].argmax() == 0:
                min_age=0
                max_age=2
            elif age_preds[0].argmax() == 1:
                min_age=4
                max_age=6
            elif age_preds[0].argmax() == 2:
                min_age=8
                max_age=12
            elif age_preds[0].argmax() == 3:
                min_age=15
                max_age=20
            elif age_preds[0].argmax() == 4:
                min_age=25
                max_age=32
            elif age_preds[0].argmax() == 5:
                min_age=38
                max_age=43
            elif age_preds[0].argmax() == 6:
                min_age=48
                max_age=53
            elif age_preds[0].argmax() == 7:
                min_age=60
                max_age=100
            # Stažení omezeni vstupu pro věk a pohlaví z REST API
            permissions = requests.get('https://akela.mendelu.cz/~xkremece/avtk/public/index.php/permissions')
            data = permissions.json()
            omezenipohlavi = str(data[0]['omezeni_pohlavi'])
            omezenivekumin = int(data[0]['omezeni_veku_min'])
            omezenivekumax = int(data[0]['omezeni_veku_max'])
            print(omezenipohlavi)
            print(omezenivekumin)
            print(omezenivekumax)
            print(data)
            # Pokud je odhadované pohlaví stejné jako omezované pohlaví, rozsvítí se červená LED dioda
            if gender == omezenipohlavi:
              digitalWrite(ledC,1)
              digitalWrite(ledM,0)
            elif min_age == omezenivekumin and max_age == omezenivekumax:
              digitalWrite(ledC,1)
              digitalWrite(ledM,0)
            else:
              digitalWrite(ledC,0)
              digitalWrite(ledM,1)
            # Posilaní dat na REST API
            payload = {'gender': gender,'min_age': min_age,'max_age': max_age}
            response = requests.post('https://akela.mendelu.cz/~xkremece/avtk/public/index.php/new', json=payload)
            print(response) 
        #Vykresli obraz z webcamery a vykresli čtverec kolem obličeje     
        cv2.imshow("Image", image)

        key = cv2.waitKey(1) & 0xFF
      
        # Zahodí už zpracovaný snimek
        rawCapture.truncate(0)
      
        # Ukonči cyklus po zmačknuti 'q' 
        if key == ord("q"):
           break
# Spuštění zachytovací smyčky 
if __name__ == '__main__':
    age_net, gender_net = initialize_caffe_model()
    capture_loop(age_net, gender_net)

